#include <stdio.h>
#include <time.h>
#include <stdint.h>
/*
For comparing 2 files that are each > 1 MB it takes about 46-50 ms
*/
uint32_t compare(FILE *f1, FILE *f2);
void print_16_bytes_binary(FILE *f, uint32_t Byte_Offset);
void print_16_bytes_hex(FILE *f, uint32_t Byte_Offset);
clock_t t1, t2;

int main(int argc, char **argv){
	FILE *f1, *f2;
	uint32_t Byte_Offset = 0;
	double time_to_compare = 0;
	//Check if there are at least two files given
	if(argc < 3){
		printf("Not enough files..\n");
	} else {
		f1 = fopen(argv[1], "rb");
		f2 = fopen(argv[2], "rb");
		//Ensure the files are there and we can access them
		if((f1 != NULL) && (f2 != NULL)){
			t1 = clock(); 
			Byte_Offset = compare(f1, f2);
			t2 = clock();
			//Convert the difference to MS
			time_to_compare = (double)(((double)(t2-t1)/(double)CLOCKS_PER_SEC)*1000);
			printf("Time_To_Compare: %f ms\n", time_to_compare);
			printf("Byte_Offset: %x\n", Byte_Offset);
			printf("File1:");
			print_16_bytes_binary(f1, Byte_Offset);
			printf(" File2:");
			print_16_bytes_hex(f2, Byte_Offset);
			printf("\n");
		} else {
			printf("Cannot open files...\n");
		}
	}
	fclose(f1);
	fclose(f2);
	return 0;
}

void print_16_bytes_binary(FILE *f, uint32_t Byte_Offset){
	fseek(f, Byte_Offset, SEEK_SET);
	uint8_t bit1, i = 0;
	while((!feof(f)) && (i < 128)) {
		bit1 = fgetc(f);
		i++;
		if(bit1 == '0' || bit1 == '1'){
			printf("%c", bit1);
		}
	}
}

void print_16_bytes_hex(FILE *f, uint32_t Byte_Offset){
	fseek(f, Byte_Offset, SEEK_SET);
	uint8_t bit1 = 0x00, i = 0, temp_bit = 0x0;
	while((!feof(f)) && (i < 32)) {
		for(uint8_t j = 1; j < 5 && (!feof(f)); j++){
			temp_bit = (fgetc(f)- '0');
			//Double check if were reading a bit
			if(temp_bit == 0 || temp_bit == 1){
				bit1 |= ((temp_bit) << (4-j));
			} else {
				//If were not reading a bit were at the end so end loops and reverse last hex character
				bit1 >>= 4-j+1;
				j = 5;
				i = 32;
			}
		}
		printf("%x", bit1);
		bit1 = 0x00;
		i++;
	}
}

uint32_t compare(FILE *f1, FILE *f2){
	uint32_t Byte_Offset = 0;
	char bit1, bit2;
	//Check if either file has reached the end by comparing it to EOF which is -1
	while(!feof(f1) && !feof(f2)){
		bit1 = fgetc(f1);
		bit2 = fgetc(f2);
		//Once a bit differs we can store th Byte_Offset and exit loop
		if(bit1 != bit2){
			Byte_Offset = ftell(f1);
			break;
		}
	}
	return Byte_Offset;
}
